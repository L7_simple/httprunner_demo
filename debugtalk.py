import os
import time

import requests
import yaml
from httprunner import __version__


def get_httprunner_version():
    return __version__


def sum_two(m, n):
    return m + n


def sleep(n_secs):
    time.sleep(n_secs)


def parameterizes(datapath):
    file_path = os.path.dirname(__file__)
    with open(file_path + datapath, encoding='utf-8') as file:
        content = yaml.load(file.read())
        return content


def dict_get_param(dict, name):
    return dict[name]


def new_name(name1):
    return name1 + '2'


def new_mobile(mobile):
    return str(int(mobile)+1000)


def get_token():
    corp = parameterizes('/data/corp.yaml')
    url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
    r = requests.get(url, params=corp)
    return r.json()['access_token']


# if __name__=='__main__':
#     print(get_token())
