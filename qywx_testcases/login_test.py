# coding=utf-8
# @time     :2021/12/22
# @Author   :Simple
# @FileName :login_test
# @Project  :demo

from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


class TestLogin(HttpRunner):
    config = Config("testcase description").verify(False).variables(**{
        "corpid_corpsecret": "${parameterizes(/data/corp.yaml)}",
        "url": "https://qyapi.weixin.qq.com"
    }).base_url("$url").export("access_token")

    teststeps = [
        Step(
            RunRequest("get token")
            .get("/cgi-bin/gettoken")
            .with_params(**{
                "corpid": "${dict_get_param($corpid_corpsecret,corpid)}",
                "corpsecret": "${dict_get_param($corpid_corpsecret,corpsecret)}",
            })
            .extract()
            .with_jmespath("body.access_token", "access_token")
            .validate()
            .assert_equal("status_code", 200)
            .assert_equal("body.errmsg", "ok")
        ),
    ]


if __name__ == "__main__":
    TestLogin().test_start()

