# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :add_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestAddUser(HttpRunner):

    config = Config("create user testcase").verify(False).variables(**{
        "userid": "test",
        "name": "测试",
        "department": [11],
        "mobile": "17300000023",
        "access_token": "",
        "url": "https://qyapi.weixin.qq.com",
        "errmsg_add": "created",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("create user step")
            .post("/cgi-bin/user/create")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "userid": "$userid",
                    "name": "$name",
                    "department": "$department",
                    "mobile": "$mobile",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg_add")
        ),
    ]


if __name__ == "__main__":
    TestAddUser.test_start()

