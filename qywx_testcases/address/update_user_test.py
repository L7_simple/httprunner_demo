# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :update_user_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase
from qywx_testcases.address.get_user_test import TestGetUser as Get


class TestUpdateUser(HttpRunner):

    config = Config("update user testcase").verify(False).variables(**{
        "userid": "test1",
        "name2": "测试1",
        "department": [11],
        "mobile2": "17300000110",
        "access_token": "",
        "url": "https://qyapi.weixin.qq.com",
        "errmsg_update": "updated"
    }).base_url("$url")

    # print("update user_name: " + config.__getattribute__("name"))

    teststeps = [
        Step(
            RunRequest("update user step")
            .post("/cgi-bin/user/update")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "userid": "$userid",
                    "name": "$name2",
                    "department": "$department",
                    "mobile": "$mobile2",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg_update")
        ),
        Step(
            RunTestCase("get user information")
            .with_variables(
                **{
                    "userid": "$userid",
                    "name": "$name2",
                    "mobile": "$mobile2",
                    "access_token": "$access_token",
                }
            )
            .call(Get)
        ),
    ]


if __name__ == "__main__":
    TestUpdateUser.test_start()

