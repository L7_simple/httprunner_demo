# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :batchdelete_user_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestBatchDeleteUser(HttpRunner):

    config = Config("batch delete user testcase").verify(False).variables(**{
        "useridlist": ["test1"],
        "access_token": "",
        "url": "",
        "errmsg": "deleted",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("batch delete user step")
            .get("/cgi-bin/user/batchdelete")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json({
                "useridlist": "$useridlist"
            })
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestBatchDeleteUser.test_start()

