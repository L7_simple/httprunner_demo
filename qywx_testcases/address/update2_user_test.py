# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :update_user_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestUpdateUser(HttpRunner):

    config = Config("update user testcase").verify(False).variables(**{
        "userid": "test1",
        "name": "测试1",
        "department": [11],
        "mobile": "17300000110",
        "access_token": "",
        "url": "https://qyapi.weixin.qq.com",
        "errmsg_update": "updated",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("update user step")
            .post("/cgi-bin/user/update")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "userid": "$userid",
                    "name": "$name",
                    "department": "$department",
                    "mobile": "$mobile",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg_update")
        ),
    ]


if __name__ == "__main__":
    TestUpdateUser.test_start()

