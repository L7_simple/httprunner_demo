# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :get_user_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestGetUser(HttpRunner):

    config = Config("get user testcase").verify(False).variables(**{
        "userid": "test1",
        "name": "测试1",
        "mobile": "17300000100",
        "access_token": "",
        "url": "https://qyapi.weixin.qq.com",
        "errmsg_get": "ok",
    }).base_url("$url")\
        .export("get_name")\
        .export("get_mobile")\
        .export("get_department")

    teststeps = [
        Step(
            RunRequest("get user step")
            .get("/cgi-bin/user/get")
            .with_params(**{
                "access_token": "$access_token",
                "userid": "$userid",
            })
            .extract()
            .with_jmespath("body.name", "get_name")
            .with_jmespath("body.mobile", "get_mobile")
            .with_jmespath("body.department", "get_department")
            .validate()
            .assert_equal("body.errmsg", "$errmsg_get")
            .assert_equal("body.name", "$name")
            .assert_equal("body.mobile", "$mobile")
        ),
    ]


if __name__ == "__main__":
    TestGetUser.test_start()

