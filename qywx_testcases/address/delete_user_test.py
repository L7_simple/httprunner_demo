# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :delete_user_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestDeleteUser(HttpRunner):

    config = Config("delete user testcase").verify(False).variables(**{
        "userid": "test1",
        "access_token": "",
        "url": "https://qyapi.weixin.qq.com",
        "errmsg_del": "deleted",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("delete user step")
            .get("/cgi-bin/user/delete")
            .with_params(**{
                "access_token": "$access_token",
                "userid": "$userid",
            })
            .validate()
            .assert_equal("body.errmsg", "$errmsg_del")
        ),
    ]


if __name__ == "__main__":
    TestDeleteUser.test_start()

