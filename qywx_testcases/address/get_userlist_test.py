# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :get_userlist_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestGetUserList(HttpRunner):

    config = Config("get user list testcase").verify(False).variables(**{
        "department_id": "2",
        "fetch_child": "0",  # 1-递归获取，0-只获取本部门
        "access_token": "",
        "url": "",
        "errmsg": "ok",
    }).base_url("$url")\
        .export("userlist")

    teststeps = [
        Step(
            RunRequest("get user list step")
            .get("/cgi-bin/user/fetch_child")
            .with_params(**{
                "access_token": "$access_token",
                "department_id": "$department_id",
                "fetch_child": "$fetch_child",
            })
            .extract()
            # list每个元素是dict{userid/name/department/open_userid}
            .with_jmespath("body.userlist", "userlist")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestGetUserList.test_start()

