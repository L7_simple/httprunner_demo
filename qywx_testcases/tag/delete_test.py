# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :delete_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestDeleteTag(HttpRunner):

    config = Config("delete tag testcase").verify(False).variables(**{
        "tagid": "10",
        "access_token": "",
        "url": "",
        "errmsg": "deleted"
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("delete tag step")
            .get("/cgi-bin/tag/delete")
            .with_params(**{
                "access_token": "$access_token",
                "tagid": "$tagid",
            })
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestDeleteTag.test_start()

