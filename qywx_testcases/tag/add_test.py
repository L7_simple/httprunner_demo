# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :add_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestAddTag(HttpRunner):

    config = Config("create tag testcase").verify(False).variables(**{
        "tagname": "测试",
        "tagid": 2,
        "access_token": "",
        "url": "",
        "errmsg": "created"
    }).base_url("$url").export("tagid")

    teststeps = [
        Step(
            RunRequest("create tag step")
            .post("/cgi-bin/tag/create")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "tagid": "$tagid",
                    "tagname": "$tagname",
                }
            )
            .extract()
            .with_jmespath("body.tagid", "tagid")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestAddTag.test_start()

