# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :get_list_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestGetTagList(HttpRunner):

    config = Config("get tag list testcase").verify(False).variables(**{
        "access_token": "",
        "url": "",
        "errmsg": "ok"
    }).base_url("$url")\
        .export("tag_list")

    teststeps = [
        Step(
            RunRequest("get tag list step")
            .get("/cgi-bin/tag/list")
            .with_params(**{
                "access_token": "$access_token",
            })
            .extract()
            # list每个元素是dict{tagid/tagname}
            .with_jmespath("body.taglist", "tag_list")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestGetTagList.test_start()

