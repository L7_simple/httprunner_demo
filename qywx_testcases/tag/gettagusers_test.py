# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :gettagusers_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestGetTagUsers(HttpRunner):

    config = Config("get tag users testcase").verify(False).variables(**{
        "access_token": "",
        "url": "",
        "errmsg": "ok"
    }).base_url("$url")\
        .export("tag_list")\
        .export("partylist")

    teststeps = [
        Step(
            RunRequest("get tag users step")
            .get("/cgi-bin/tag/get")
            .with_params(**{
                "access_token": "$access_token",
            })
            .extract()
            # 标签成员列表 list每个元素是dict{userid/name}
            .with_jmespath("body.userlist", "tag_user_list")
            .with_jmespath("body.partylist", "tag_part_list")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestGetTagUsers.test_start()

