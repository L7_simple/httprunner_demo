# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :deltagusers_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestDelTagUsers(HttpRunner):

    config = Config("delete tag users testcase").verify(False).variables(**{
        "userlist": [],
        "tagid": 2,
        "partylist": [],
        "access_token": "",
        "url": "",
        "errmsg": "deleted",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("delete tag users step")
            .post("/cgi-bin/tag/deltagusers")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "tagid": "$tagid",
                    "userlist": "$userlist",
                    "partylist": "$partylist",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestDelTagUsers.test_start()
