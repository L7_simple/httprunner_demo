# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :update_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestUpdateTag(HttpRunner):

    config = Config("update tag testcase").verify(False).variables(**{
        "tagname": "测试",
        "tagid": 2,
        "access_token": "",
        "url": "",
        "errmsg": "updated",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("update tag step")
            .post("/cgi-bin/tag/update")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "tagid": "$tagid",
                    "tagname": "$tagname",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestUpdateTag.test_start()

