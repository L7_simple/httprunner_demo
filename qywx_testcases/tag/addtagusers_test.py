# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :addtagusers_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestAddTagUsers(HttpRunner):

    config = Config("add tag users testcase").verify(False).variables(**{
        "userlist": [],
        "tagid": 2,
        "partylist": [],
        "access_token": "",
        "url": "",
        "errmsg": "ok"
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("add tag users step")
            .post("/cgi-bin/tag/addtagusers")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "tagid": "$tagid",
                    "userlist": "$userlist",
                    "partylist": "$partylist",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestAddTagUsers.test_start()

