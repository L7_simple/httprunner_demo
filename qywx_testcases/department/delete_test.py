# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :delete_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestDeleteDepartment(HttpRunner):

    config = Config("delete department testcase").verify(False).variables(**{
        "id": "10",
        "access_token": "",
        "url": "",
        "errmsg": "deleted"
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("delete department step")
            .get("/cgi-bin/department/delete")
            .with_params(**{
                "access_token": "$access_token",
                "id": "$id",
            })
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestDeleteDepartment.test_start()

