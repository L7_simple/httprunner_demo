# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :update_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestUpdateDepartment(HttpRunner):

    config = Config("update department testcase").verify(False).variables(**{
        "name": "测试",
        "parentid": 2,
        "access_token": "",
        "url": "",
        "errmsg": "updated",
    }).base_url("$url")

    teststeps = [
        Step(
            RunRequest("update department step")
            .post("/cgi-bin/department/update")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "parentid": "$parentid",
                    "name": "$name",
                }
            )
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestUpdateDepartment.test_start()

