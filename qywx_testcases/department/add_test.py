# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :add_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestAddDepartment(HttpRunner):

    config = Config("create department testcase").verify(False).variables(**{
        "name": "测试",
        "parentid": 2,
        "access_token": "",
        "url": "",
        "errmsg": "created"
    }).base_url("$url").export("department_id")

    teststeps = [
        Step(
            RunRequest("create department step")
            .post("/cgi-bin/department/create")
            .with_params(**{
                "access_token": "$access_token",
            })
            .with_json(
                {
                    "parentid": "$parentid",
                    "name": "$name",
                }
            )
            .extract()
            .with_jmespath("body.id", "department_id")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestAddDepartment.test_start()

