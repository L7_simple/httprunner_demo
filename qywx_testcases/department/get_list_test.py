# coding=utf-8
# @time     :2021/12/23
# @Author   :Simple
# @FileName :get_list_test
# @Project  :demo
from httprunner import HttpRunner, Config, Step, RunRequest


class TestGetDepartmentList(HttpRunner):

    config = Config("get department list testcase").verify(False).variables(**{
        "id": "2",
        "fetch_child": "0",  # 1-递归获取，0-只获取本部门
        "access_token": "",
        "url": "",
        "errmsg": "ok"
    }).base_url("$url")\
        .export("department_list")

    teststeps = [
        Step(
            RunRequest("get department list step")
            .get("/cgi-bin/department/list")
            .with_params(**{
                "access_token": "$access_token",
                "id": "$id",
            })
            .extract()
            # list每个元素是dict{id/name/name_en/department_leader/parentid/order}
            .with_jmespath("body.department", "department_list")
            .validate()
            .assert_equal("body.errmsg", "$errmsg")
        ),
    ]


if __name__ == "__main__":
    TestGetDepartmentList.test_start()

