# coding=utf-8
# @time     :2021/12/22
# @Author   :Simple
# @FileName :address_test
# @Project  :demo
import pytest
from httprunner import HttpRunner, Config, Step, RunTestCase, Parameters
from qywx_testcases.address.add_test import TestAddUser as Add
from qywx_testcases.address.get_user_test import TestGetUser as Get
from qywx_testcases.address.update_user_test import TestUpdateUser as Update
from qywx_testcases.address.delete_user_test import TestDeleteUser as Delete
from qywx_testcases.login_test import TestLogin as Login


class TestAddress(HttpRunner):
    base_url = "https://qyapi.weixin.qq.com"

    @pytest.mark.parametrize(
        "param",
        Parameters(
            {
                "userid-name-department-mobile": [
                    ["test1", "测试1", [11], '17300000100'],
                    ["test2", "测试2", [11], '17300000101'],
                    ["test3", "测试3", [11], '17300000102'],
                    ["test4", "测试4", [11], '17300000103'],
                ],
            }
        ),
    )
    def test_start(self, param):
        super().test_start(param)

    config = Config("test address testcase").verify(False) \
        .variables(**{
        "corpid_corpsecret": "${parameterizes(/data/corp.yaml)}",
    }) \
        .base_url(base_url)

    teststeps = [
        Step(
            RunTestCase("login")
                .with_variables(
                **{
                    "corpid": "${dict_get_param($corpid_corpsecret,corpid)}",
                    "corpsecret": "${dict_get_param($corpid_corpsecret,corpsecret)}",
                }
            )
                .call(Login)
                .export("access_token")
        ),
        Step(
            RunTestCase("add user information")
                .with_variables(
                **{
                    "userid": "$userid",
                    "name": "$name",
                    "department": "$department",
                    "access_token": "$access_token",
                    "mobile": "$mobile",
                }
            )
                .call(Add)
        ),
        Step(
            RunTestCase("get user information")
                .with_variables(
                **{
                    "userid": "$userid",
                    "name": "$name",
                    "mobile": "$mobile",
                    "access_token": "$access_token",
                }
            )
                .call(Get)
                .export("get_name")
                .export("get_mobile")
                .export("get_department")
        ),
        Step(
            RunTestCase("update user information")
                .with_variables(
                **{
                    "userid": "$userid",
                    "name2": "${new_name($name)}",  # name同时处理同时存储会报错
                    "mobile2": "${new_mobile($mobile)}",
                    "department": "$department",
                    "access_token": "$access_token",
                }
            )
                .call(Update)
        ),
        Step(
            RunTestCase("delete user information")
            .with_variables(
                **{
                    "userid": "$userid",
                    "access_token": "$access_token",
                }
            )
            .call(Delete)
        ),
    ]


if __name__ == "__main__":
    TestAddress().test_start()
