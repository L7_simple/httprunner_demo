# demo_httprunner

#### 介绍

使用HttpRunner做个demo接口测试和性能测试

#### 软件架构

- data: 存放登录密码
- demo_testcase: httprunner框架的demo
- har: 存放录制接口导出har文件、使用har2case转换的对应har的py文件
- logs：日志文件
- qywx_testcases: 企业微信接口测试用例
- reports: allure测试报告
- debugtalk.py 常用方法

#### 安装教程

1.  python环境
    - 支持3.6/3.7/3.8/3.9
2.  httprunner
```
pip install httprunner
pip install allure-pytest
pip install locust  
```
[HttpRunner V3.x Docs](https://docs.httprunner.org/)

#### 使用说明

1.  获取企业微信通讯录应用信息
    - 创建data/corp.yaml，存放企业微信的corpid、corpsecret
2.  执行测试用例
```
httprunner run qywx_testcases/address_test.py
```
3.  测试并生成allure报告
```
httprunner run qywx_testcases/address_test.py --alluredir=reports
allure serve reports
```
4.  locust性能测试
```
locusts -f qywx_testcases/address_test.py
```
